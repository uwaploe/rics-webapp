#!/usr/bin/env python

from flask.ext.script import Manager
from rtrui.app import app


manager = Manager(app)
app.config['DEBUG'] = True

if __name__ == '__main__':
    manager.run()
