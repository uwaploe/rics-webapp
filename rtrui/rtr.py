#!/usr/bin/env python
#
# Web application gateway to the ZeroMQ based RTR Controller.
#
from flask import Blueprint, current_app, request, \
    json, jsonify
import zmq
import time


rtr_ctl = Blueprint('rtr', __name__)


def ctl_gateway(url, msg, reformat=None):
    """
    Gateway to the RTR Controller process.

    :param url: ZeroMQ URL for the Controller endpoint
    :param msg: message to send
    :param reformat: callable to reformat the Controller response
    :returns: Flask response.
    """
    fmt = reformat or (lambda x: x)
    timeout = current_app.config.get('CONTROLLER_TIMEOUT', 5000)
    ctx = zmq.Context()
    ctl = ctx.socket(zmq.DEALER)
    ctl.connect(url)

    poller = zmq.Poller()
    poller.register(ctl, zmq.POLLIN)

    try:
        ctl.send_multipart(msg, zmq.NOBLOCK)
    except zmq.ZMQError:
        current_app.logger.warn('Controller offline')
        resp = (json.dumps({'reply': 'ERROR',
                            'attrs': 'Controller offline'}),
                503,
                {'Content-type': 'application/json'})
    else:
        socks = dict(poller.poll(timeout=timeout))
        if socks.get(ctl) == zmq.POLLIN:
            _, code, args = ctl.recv_multipart()
            if code == b'ERROR':
                resp = jsonify(reply=code,
                               attrs=args)
            else:
                resp = jsonify(reply=code,
                               attrs=fmt(args))
        else:
            resp = (json.dumps({'reply': 'ERROR',
                                'attrs': 'No response'}),
                    503,
                    {'Content-type': 'application/json'})
    ctl.close(linger=0)
    return resp


@rtr_ctl.route('/cfg/<int:rtr_id>', methods=['GET', 'PUT'])
@rtr_ctl.route('/cfg/<int:rtr_id>/<path:key>', methods=['GET', 'PUT'])
def cfg(rtr_id, key=None):
    rtr_id = str(rtr_id)
    if request.method == 'PUT':
        data = request.get_json(force=True)
        if key:
            args = '='.join([key, str(data)]).encode('us-ascii')
        else:
            args = ' '.join(['='.join([str(k), str(v)])
                             for k, v in data.items()])
        msg = [json.dumps(time.time()),
               rtr_id.encode('us-ascii'),
               b'SET',
               args]
    else:
        if key:
            msg = [json.dumps(time.time()),
                   rtr_id.encode('us-ascii'),
                   b'GET',
                   key.encode('us-ascii')]
        else:
            msg = [json.dumps(time.time()),
                   b'',
                   b'CFG',
                   rtr_id.encode('us-ascii')]
    return ctl_gateway(current_app.config.get('CONTROLLER_URL',
                                              'tcp://localhost:10133'),
                       msg,
                       lambda args: dict([a.split('=') for a in args.split()]))


@rtr_ctl.route('/nodes')
def nodes():
    msg = [json.dumps(time.time()),
           b'',
           b'NODES',
           b'']
    return ctl_gateway(current_app.config.get('CONTROLLER_URL',
                                              'tcp://localhost:10133'),
                       msg,
                       lambda args: args.split())


@rtr_ctl.route('/raw/<int:rtr_id>/<cmd>', methods=['PUT'])
def rawcmd(rtr_id, cmd):
    rtr_id = str(rtr_id)
    if len(request.data):
        data = request.get_json(force=True)
    else:
        data = ''

    msg = [json.dumps(time.time()),
           rtr_id.encode('us-ascii'),
           cmd.upper().encode('us-ascii'),
           data.encode('us-ascii')]
    return ctl_gateway(current_app.config.get('CONTROLLER_URL',
                                              'tcp://localhost:10133'),
                       msg)
