// Parse a Dmm object from a string of the form:
//
//    DDD-MM.mmmmH
define(["./dmm"], function(Dmm) {
    return function(str) {
        var re = /([0-9]*)-([0-9\.]*)([NSEW])/i;
        var m = re.exec(str);
        var dmm = new Dmm();

        if(m == null)
            throw "Invalid input: " + str;

        dmm.deg = parseFloat(m[1]);
        dmm.min = parseFloat(m[2]);
        if(m[3].toUpperCase() == "S" || m[3].toUpperCase() == "W") {
            dmm.deg = -dmm.deg;
            dmm.min = -dmm.min;
        }

        return dmm;
    };
});
