// Module loader
require.config({
    paths: {
        d3: "lib/d3.min",
        underscore: "lib/underscore-min",
        moment: "lib/moment.min",
        domReady: "lib/domReady",
        geopoint: "./geopoint",
        parsedmm: "./parseDmm"
    },
    config: {
        moment: {
            noGlobal: true
        }
    }
});

require(["app", "domReady!"], function(App, doc) {
    App.initialize("/stream");
});
