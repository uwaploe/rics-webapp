//
// RTR UI application.
//
define(["d3", "geopoint", "underscore", "moment"],
       function(d3, GeoPoint, _, M) {

           // Fill a table with the contents of a message
           function filltable(rec, tag) {
               var t = rec["_msgtime"];
               _.each(rec, function(v, k) {
                   var id = "#" + k + tag;
                   var sel = d3.select(id);
                   if(!sel.empty()) {
                       var dt = t - sel.datum();
                       if(isNaN(dt) || dt > 0) {
                           sel.datum(t)
                               .classed("timed", true)
                               .style("color", "inherit")
                               .text(v.toString());
                       }
                   }
               });
           }

           // Highlight all "stale" data fields
           function highlight_data(maxage) {
               d3.selectAll(".timed")
                   .style("color", function(d, i) {
                       return (Date.now() - d) > maxage ? "red" : null;
                   });
           }

           function update_cfg(rtrid, data, callback) {
               d3.xhr("/rtr/cfg/" + rtrid)
                   .header("Content-Type", "application/json")
                   .send("PUT",
                         JSON.stringify(data),
                         callback);
           }

           // Have we received data from this node in the past 30 seconds?
           function isactive(rtrid) {
               var id = "#time_rtr_" + rtrid;
               return ((Date.now() - d3.select(id).datum()) <= 30000);
           }

           // Update the display to show the debug state of a Node.
           function show_debug_state(rtrid, state) {
               var sel = "#rtr_" + rtrid;
               if(state) {
                   d3.selectAll(".debug" + rtrid)
                       .each(function(d, i) {
                           d3.select(this).style("visibility", "visible");
                       });
                   d3.select(sel)
                       .datum("debug")
                       .style("border", "4px solid red");
               } else {
                   d3.selectAll(".debug" + rtrid)
                       .each(function(d, i) {
                           d3.select(this).style("visibility", "hidden");
                       });
                   d3.select(sel)
                       .datum("")
                       .style("border", "none");
               }
           }

           // Toggle debugging output from a Node.
           function debug_toggle(rtrid) {
               var data = {};
               if(isactive(rtrid)) {
                   var sel = "#rtr_" + rtrid;
                   if(d3.select(sel).datum() != "debug") {
                       data["general/debug"] = 1;
                       update_cfg(rtrid, data, function(err, resp) {
                           show_debug_state(rtrid, true);
                        });
                   } else {
                       data["general/debug"] = 0;
                       update_cfg(rtrid, data, function(err, resp) {
                           show_debug_state(rtrid, false);
                       });
                   }
               }
           }


           var tformat = "YYYY-MM-DD HH:mm:ss";
           // Buffer for timestamp data
           var ts_platform = [[], [], [], []];
           var ts_unit = [[], [], [], []];
           var maxpoints = 50;

           var initialize = function(url) {
               // Install click handlers to enable/disable debugging and
               // check the initial debug state
               _.each([1, 2, 3, 4], function(v, k) {
                   var sel = "#rtr_" + v;
                   d3.select(sel)
                       .on("click", function(d, i) {
                           debug_toggle(v);
                       });
                   d3.json("/rtr/cfg/" + v, function(err, resp) {
                       if(resp != undefined) {
                           show_debug_state(v,
                                            resp.attrs["general/debug"] == "1");
                       }
                   });
               });

               var task = setInterval(highlight_data, 5000, 30000);

               // Register the SSE handlers
               var src = new EventSource(url);

               src.addEventListener("GPS", function(e) {
                   var data = JSON.parse(e.data);
                   var tag = "_rtr_" + data.rtr;
                   var rec = {
                       _msgtime: data.time * 1000,
                       time: M(data.time * 1000).utc().format(tformat) + "Z",
                       gps: (new GeoPoint(data.lat, data.lon)).tostring()
                   };

                   filltable(rec, tag);
                   if(data.mode < 2) {
                       d3.select("#gps" + tag)
                           .style("background-color", "#b92c28");
                   } else {
                       d3.select("#gps" + tag)
                           .style("background-color", null);
                   }
               });

               src.addEventListener("ENG", function(e) {
                   var data = JSON.parse(e.data);
                   var tag = "_rtr_" + data.rtr;
                   var rec = {
                       _msgtime: data.time * 1000,
                       time: M(data.time * 1000).utc().format(tformat) + "Z",
                       vbatt: data.vbatt,
                       ibatt: data.ibatt,
                       temp: data.temp
                   };

                   filltable(rec, tag);
               });

               src.addEventListener("XYPOS", function(e) {
                   var data = JSON.parse(e.data);
                   var tag = "_rtr_" + data.rtr;
                   var x = Number(data.x);
                   var y = Number(data.y);
                   var rec = {
                       _msgtime: data.time * 1000,
                       time: M(data.time * 1000).utc().format(tformat) + "Z",
                       grid: [x.toFixed(1), y.toFixed(1)]
                   };

                   filltable(rec, tag);
               });

               src.addEventListener("DIAG", function(e) {
                   var data = JSON.parse(e.data);
                   var tag = "_rtr_" + data.rtr;
                   var rec = {};
                   _.each(data.contents, function(v, k) {
                       rec[k.toLowerCase()] = v;
                   });

                   rec["time"] = M(data.time * 1000).utc().format(tformat) + "Z";
                   rec["_msgtime"] = data.time * 1000;
                   filltable(rec, tag);
               });

               src.addEventListener("TS", function(e) {
                   var data = JSON.parse(e.data);
                   if(data.rtr < 0x80) {
                       var i = data.rtr - 1;
                       var x = data.time * 1000;
                       var y = data.platform[0];

                       if(y >= 0) {
                           if(ts_platform[i].push([x, y]) > maxpoints) {
                               ts_platform[i].shift();
                           }
                       }
                       y = data.unit[0];
                       if(y >= 0) {
                           if(ts_unit[i].push([x, y]) > maxpoints) {
                               ts_unit[i].shift();
                           }
                       }

                       var rec = {
                           _msgtime: x,
                           time: M(x).utc().format(tformat) + "Z",
                           platform: data.platform,
                           unit: data.unit
                       };
                       filltable(rec, "_rtr_" + data.rtr);
                   }
               });
           };

           return {
               initialize: initialize
           };
});
