#!/usr/bin/env python

from flask import Flask, render_template
from redis import StrictRedis
from sse import Sse
import json
from .rtr import rtr_ctl
from .settings import debugvars


app = Flask(__name__)
app.config['CONTROLLER_URL'] = 'tcp://localhost:10133'
# Response timeout in milliseconds
app.config['CONTROLLER_TIMEOUT'] = 4000
app.config['DATA_CHANNEL'] = 'rics'
app.register_blueprint(rtr_ctl, url_prefix='/rtr')


class SseStream(object):
    def __init__(self, conn, channel):
        self.pubsub = conn.pubsub()
        self.pubsub.subscribe(channel)

    def __iter__(self):
        """
        For each message read from the Redis channel, do the
        following:

        - decode the JSON array containing the type tag and
          the data record.
        - pass the Sse object the type tags as the event name
          along with the JSON-encoded data record.
        """
        sse = Sse()
        for data in sse:
            yield data.encode('u8')
        for message in self.pubsub.listen():
            if message['type'] == 'message':
                event, data = json.loads(message['data'])
                sse.add_message(event, json.dumps(data))
                for data in sse:
                    yield data.encode('u8')


@app.route('/stream')
def stream():
    conn = StrictRedis()
    return app.response_class(
        SseStream(conn, app.config.get('DATA_CHANNEL', 'rics')),
        direct_passthrough=True,
        mimetype='text/event-stream')


@app.route('/')
def index():
    return render_template('index.html',
                           nodes=[1, 2, 3, 4],
                           debugvars=debugvars)
