debugvars = [
    {
        'name': 'threshold_a',
        'label': 'Threshold A',
        'doc': 'Threshold setting for platform signal'
    },
    {
        'name': 'threshold_b',
        'label': 'Threshold B',
        'doc': 'Threshold setting for platform signal'
    },
    {
        'name': 'peakaout',
        'label': 'Peak A',
        'doc': 'Peak output level for platform signal'
    },
    {
        'name': 'peakbout',
        'label': 'Peak B',
        'doc': 'Peak output level for unit signal'
    },
    {
        'name': 'timeoffset',
        'label': 'T offset'
    }
]
