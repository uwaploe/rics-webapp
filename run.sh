#!/bin/bash
#
# Run the app under Gunicorn
#
gunicorn -w 4 -k eventlet -t 30 \
         -b 0.0.0.0:5000 \
         rtrui.app:app
